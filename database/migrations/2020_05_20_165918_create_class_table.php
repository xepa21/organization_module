<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('college_id')->unsigned();
            $table->foreign('college_id')->references('id')->on('college');
            $table->string('contact_number');
            $table->string('email')->unique();
            $table->double('price', 11, 2);
            $table->text('description');
            $table->string('syllabus', 200);         
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class');
    }
}
