<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
            ['name' => 'U.V. Patel College of Engg'],
            ['name' => 'Changa University'],
            ['name' => 'Sikkim Manipal University'],
            ['name' => 'SAL Institute'],
            ['name' => 'LDRP College of Engg'],
            ['name' => 'L. J. College of Engg'],
            ['name' => 'NSIT College of Engg'],
            ['name' => 'DIICT College of Management'],

        ];

        foreach ($items as $item) {
            \App\College::create($item);
        }
    }
}
