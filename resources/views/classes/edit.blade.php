@extends('layouts.app')

@section('content')
    <h3 class="page-title">Class</h3>
     {!! Form::model($class, ['method' => 'PUT', 'route' => ['class.update', $class->id], 'files' => true,]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            Create
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('college', 'College', ['class' => 'control-label']) !!}
                    {!! Form::select('college_id', $colleges, old('college'), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                </div>

                <div class="col-xs-6 form-group">
                    {!! Html::decode(Form::label('title', 'title <span class="text-danger">*</span>', ['class' => 'control-label'])) !!}
                    {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('title'))
                        <p class="help-block">
                            {{ $errors->first('title') }}
                        </p>
                    @endif
                </div>              
            
                
            </div>

            <div class="row">
                 <div class="col-xs-6 form-group">
                    {!! Html::decode(Form::label('contact_number', 'Contact Number <span class="text-danger">*</span>', ['class' => 'control-label'])) !!}
                    {!! Form::number('contact_number', old('contact_number'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('contact_number'))
                        <p class="help-block">
                            {{ $errors->first('contact_number') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
                    {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('email'))
                        <p class="help-block">
                            {{ $errors->first('email') }}
                        </p>
                    @endif
                </div>               
                
                
            </div>
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Html::decode(Form::label('price', 'Price <span class="text-danger">*</span>', ['class' => 'control-label'])) !!}
                    {!! Form::number('price', old('price'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('price'))
                        <p class="help-block">
                            {{ $errors->first('price') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Html::decode(Form::label('description', 'Description <span class="text-danger">*</span>', ['class' => 'control-label'])) !!}
                    {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => '', 'rows' => '5']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('description'))
                        <p class="help-block">
                            {{ $errors->first('description') }}
                        </p>
                    @endif
                </div>  
                
                 
            </div>            

            <div class="row after-add-more">
                <div class="col-xs-6 form-group">
                    {!! Html::decode(Form::label('syllabus', 'Syllabus <span class="text-danger">*</span>', ['class' => 'control-label'])) !!}
                    {!! Form::file('syllabus', ['class' => 'form-control', 'style' => 'margin-top: 4px;']) !!}
                    
                    <p class="help-block"></p>
                    @if($errors->has('syllabus'))
                        <p class="help-block">
                            {{ $errors->first('syllabus') }}
                        </p>
                    @endif
                </div>

                @if(count($levels) > 0)
                @foreach($levels as $h => $b)
                <div class="col-xs-4 form-group">
                    {!! Html::decode(Form::label('level', 'Level <span class="text-danger">*</span>', ['class' => 'control-label'])) !!}
                    {!! Form::text('level[]', $b->level_name, ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('level'))
                        <p class="help-block">
                            {{ $errors->first('level') }}
                        </p>
                    @endif
                </div>
                @endforeach
                @endif
                <div class="col-xs-2 form-group input-group-btn " style="margin-top: 25px;"> 
                <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
            </div>                
            </div>
            
            <div class="row copy " style="display: none;">
                <div class="col-xs-6 form-group control-group">
                    
                    {!! Form::text('level[]', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('level'))
                        <p class="help-block">
                            {{ $errors->first('level') }}
                        </p>
                    @endif
                
                  <button class="btn btn-danger remove " type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                </div>
            </div>
                     
    
        </div>
    </div>

    {!! Form::submit('Save', ['class' => 'btn btn-danger']) !!}
     <a href="{{ url('classes') }}" class="btn btn-default">Cancel</a>
    {!! Form::close() !!}
@stop
<script type="text/javascript" src="{{url('/public/js/jquery.min.js')}}"></script>



<script type="text/javascript">
    $(document).ready(function() {


      $(".add-more").click(function(){ 
        
          var html = $(".copy").html();
          $(".after-add-more").after(html);
      });


      $("body").on("click",".remove",function(){ 
          $(this).parents(".control-group").remove();
      });


    });


    function isNumberKey(evt,id)
    {
        try{
            var charCode = (evt.which) ? evt.which : event.keyCode;
      
            if(charCode==46){
                var txt=document.getElementById(id).value;
                if(!(txt.indexOf(".") > -1)){
        
                    return true;
                }
            }
               if(charCode == 118){
               return true;
            }
            if (charCode > 31 && (charCode < 48 || charCode > 57) )
                return false;

            return true;
        }catch(w){
            //alert(w);
        }
    }

</script>

