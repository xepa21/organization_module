@extends('layouts.app')
@section('content')
 <a href="{{ route('class.create') }}" class="btn btn-success">Add New</a>

<div class="panel-body"> 
  <div class="panel-body">   
    <div class="row">         
      <table class="table table-bordered" id="institute_table" style="width:100%">

        <thead>
          <th>College</th>
          <th>Class</th>
          <th>Levels</th>
          <th>Action</th>

        </thead>        

      </table> 
    </div>
  </div>

</div>
@endsection

<script type="text/javascript" src="{{url('/public/js/jquery.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
 

<script>

  $(document).ready(function(){

// Data table for serverside

$('#institute_table').DataTable({

  "processing": true,

  "serverSide": true,
  "pageLength": 5,

  "ajax":{

    "url": "{{ url('classList') }}",

    "dataType": "json",

    "type": "POST",

    "data":{ _token: "{{csrf_token()}}",route:'classList'}

  },

  "columns": [
  { "data": "college_name" },
  { "data": "title" },
  { "data": "levels" },
  { "data": "action" }

  ],

  aoColumnDefs: [

  {

    bSortable: false,

    aTargets: [ -1 ]

  }

  ]  



});
});
</script>
