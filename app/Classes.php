<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $table = 'class';
    protected $fillable = ['title', 'price', 'description', 'email', 'syllabus', 'college_id', 'contact_number'];
}
