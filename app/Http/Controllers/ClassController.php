<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes;
use App\ClassLevels;
use App\College;
/*use Input;
use Validator;
use Redirect;
use View;*/


class ClassController extends Controller
{
	function classes(Request $request){
		return view('classes.list');
	}

	function classList(Request $request){

		$columns = array(
			1 =>'college_name',
			2 =>'title',
			3 =>'levels',
		);  
		$totalData = Classes::count();            
		$totalFiltered = $totalData;
		$limit = $request->input('length');
		$start = $request->input('start');
		/*$order = $columns[$request->input('order.0.column')];
		$dir = $request->input('order.0.dir');*/

		if(empty($request->input('search.value')))
		{            
			$institutes = Classes::offset($start)
			->limit($limit)
			->orderBy('id', 'DESC')
			->get();
		}else {
			$search = $request->input('search.value'); 

			$institutes =  Classes::leftJoin('college', 'college.id', '=', 'class.college_id')
			->leftJoin('class_levels', 'class_levels.id', '=', 'class.id')
			->where('class.id','LIKE',"%{$search}%")
			->orWhere('class.title', 'LIKE',"%{$search}%")
			->orWhere('college.name', 'LIKE',"%{$search}%")
			->orWhere('class_levels.level_name', 'LIKE',"%{$search}%")			
			->offset($start)
			->limit($limit)
			->orderBy('class.id', 'DESC')
			->get();

			

			$totalFiltered = Classes::leftJoin('college', 'college.id', '=', 'class.college_id')
			->leftJoin('class_levels', 'class_levels.id', '=', 'class.id')
			->where('class.id','LIKE',"%{$search}%")
			->orWhere('class.title', 'LIKE',"%{$search}%")
			->orWhere('college.name', 'LIKE',"%{$search}%")
			->orWhere('class_levels.level_name', 'LIKE',"%{$search}%")			
			->count();
		}
		$data = array();
		if(!empty($institutes))
		{
			foreach ($institutes as $key=>$institute)
			{
				$getCollegeName = College::find($institute->college_id);
				$getLevels = ClassLevels::where('class_id', $institute->id)->get();
				$levelsName = [];
				if(count($getLevels) > 0) {
					foreach($getLevels as $r=>$t) {
						$levelsName[] = $t->level_name;
					}
				}
				$nestedData['college_name'] = $getCollegeName->name;
				$nestedData['title'] = $institute->title;
				$nestedData['levels'] = implode(",", $levelsName);
				$nestedData['action'] = '<a href="'.url('/class/'.$institute->id.'/delete').'" class="del"><span class="glyphicon glyphicon-trash"></span> 
				</a><a href="'.url('/class/'.$institute->id.'/edit').'" class="edit"><span class="glyphicon glyphicon-edit"></span></a>';
				$data[] = $nestedData;
			}

		}
		$json_data = array(
			"draw"            => intval($request->input('draw')),  
			"recordsTotal"    => intval($totalData),  
			"recordsFiltered" => intval($totalFiltered), 
			"data"            => $data   
		);            
		echo json_encode($json_data); 
	}

	public function create()
    {
       /* if (! Gate::allows('product_create')) {
            return abort(401);
        }*/
        
        $colleges = \App\College::get()->pluck('name', 'id')->prepend('Select College', '');
       

        return view('classes.create', compact('colleges'));
    }

    public function store(Request $request)
    {     
    	$validatedData = $request->validate([
            'title'       => 'required',
            'email' => 'required|email|unique:class,email',
            'price' => 'required',
            'description' => 'required',
            'syllabus' => 'required|max:8000|mimes:doc,docx,pdf',
            'college_id' => 'required',
            'contact_number' => 'required',
            'level' => 'required|array' 
        ]);
        
        

        $insertArray = $request->all();      
        $filename = '';
        if($request->hasFile('syllabus')) {
        	$file = $request->file('syllabus');   
			$filename = $file->getClientOriginalName();
			   
			//Move Uploaded File
			$destinationPath = 'public/uploads';
			$file->move($destinationPath,$file->getClientOriginalName());
        }

        $insertArray['syllabus'] = $filename;
        
        // dd($insertArray);
        $newClass = Classes::create($insertArray);
        $class_id = $newClass->id;    

        $levels = $request->level;
        
        foreach($levels as $k => $v) {
        	if($v != null) {
        	$addLevel = new ClassLevels;
        	$addLevel->class_id = $class_id;
        	$addLevel->level_name = $v;
        	$addLevel->save();
        	}
        }
       

        return redirect('classes');
    }

    public function edit($id)
    {
        
        
        $class = Classes::find($id);

        $colleges = \App\College::get()->pluck('name', 'id')->prepend('Select College', '');

        $levels = ClassLevels::where('class_id', $id)->get();
        
        return view('classes.edit', compact('colleges', 'levels', 'class', 'id'));
    } 

    public function update(Request $request, $id)
    {     
    	$validatedData = $request->validate([
            'title'       => 'required',
            'email' => 'required|email',
            'price' => 'required',
            'description' => 'required',
            'syllabus' => 'max:8000|mimes:doc,docx,pdf',
            'college_id' => 'required',
            'contact_number' => 'required',
            'level' => 'required|array' 
        ]);
        
        

        $insertArray = $request->all();      
        $filename = '';
        if($request->hasFile('syllabus')) {
        	$file = $request->file('syllabus');   
			$filename = $file->getClientOriginalName();
			   
			//Move Uploaded File
			$destinationPath = 'public/uploads';
			$file->move($destinationPath,$file->getClientOriginalName());
        }

        $insertArray['syllabus'] = $filename;
        
        // dd($insertArray);
        $classUpdate = Classes::findOrFail($id);
        $classUpdate->update($insertArray);
        $class_id = $id;    

        $levels = $request->level;

        $deleteOld = ClassLevels::where('class_id', $id)->delete();
        
        foreach($levels as $k => $v) {
        	if($v != null) {
        	$addLevel = new ClassLevels;
        	$addLevel->class_id = $class_id;
        	$addLevel->level_name = $v;
        	$addLevel->save();
        	}
        }
       

        return redirect('classes');
    }

    public function destroy($id)
    {
        // if (! Gate::allows('branch_delete')) {
        //     return abort(401);
        // }
        $deleteLevels = ClassLevels::where('class_id', $id)->delete();
        $class = Classes::findOrFail($id);
        if($class->syllabus != '' && file_exists('public/uploads/'.$class->syllabus)){
            unlink('public/uploads/'.$class->syllabus);
        }
        $class->delete();

        return redirect()->back();
    }         
}
