<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassLevels extends Model
{
    protected $table = 'class_levels';
    protected $fillable = ['level_name', 'class_id'];
}
